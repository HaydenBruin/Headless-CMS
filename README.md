-- This project is a WIP --

I'm building a headless CMS with NodeJS & React.

This solution will be self-hosted.

I plan to have a back-end similar to Umbraco (Looks-wise, columns on the left content on the right).

You can find the trello board here -> https://trello.com/b/x0BsHhXq/headless-cms

Interested in helping? contact me at bruinhayden@gmail.com