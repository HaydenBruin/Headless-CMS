import React, { Component } from 'react';

import ContentLayout from './components/layout';

class App extends Component {
    render() {
        return (
            <ContentLayout />
        );
    }
}

export default App;
