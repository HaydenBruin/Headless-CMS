import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';

export default class TreeRow extends Component {
    render() {
        return (
            <div>
                <NavLink to="/section/content/something/">
                    <div className={"tree-row level-" + this.props.level}><i className="fa fa-folder"></i> { this.props.title }</div>
                </NavLink>

                { this.props.children }
            </div>
        )
    }
}
