import React, { Component } from 'react'

export default class Tree extends Component {
    render() {
        return (
            <div className="tree">
                { this.props.children }
            </div>
        )
    }
}
