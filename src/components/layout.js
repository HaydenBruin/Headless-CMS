import React, { Component } from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';

import Home from './sections/home';
import General from './sections/general';
import SectionTree from './sections/sections';

class Layout extends Component {
    constructor() {
        super();

        this.sections = [
            {
                name: 'Content',
                slug: 'content',
                icon: 'fas fa-file'
            },
            {
                name: 'Media',
                slug: 'media',
                icon: 'fas fa-image'
            },
            {
                name: 'Structure',
                slug: 'structure',
                icon: 'fas fa-sitemap'
            },
            {
                name: 'Settings',
                slug: 'settings',
                icon: 'fas fa-wrench'
            }
        ];

    }

    render() {
        return (
            <div className="sections">
                <div className="sections-list">
                    <div className="head">
                        <div className="nopic">HB</div>
                    </div>
                    <ul>
                        {
                            this.sections.map(function(section, index) {
                                return (
                                    <li key={index}><NavLink to={'/section/' + section.slug + "/"}><i className={section.icon}></i>{section.name}</NavLink></li>
                                )
                            })
                        }
                    </ul>
                </div>
                <div className="sections-tree">
                    <Route path="/section/:section/" component={SectionTree} />
                </div>
                <div className="sections-content">
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/section/:section/something" component={General} />
                    </Switch>
                </div>
            </div>
        )
    }
}
export default Layout;