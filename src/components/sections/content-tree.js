import React, { Component } from 'react'

import Tree from "./../tree/tree";
import TreeRow from './../tree/tree-row';

class ContentTree extends Component {
    render() {
        return (
            <Tree>
                <TreeRow title="yourwebsite.com" level={1}>
                    <TreeRow title="Home" level={2}></TreeRow>
                    <TreeRow title="About Us" level={2}></TreeRow>
                    <TreeRow title="Pricing" level={2}></TreeRow>
                    <TreeRow title="Contact Us" level={2}></TreeRow>
                </TreeRow>
                <TreeRow title="subwebsite.com" level={1}>
                    <TreeRow title="Home" level={2}></TreeRow>
                    <TreeRow title="About Us" level={2}></TreeRow>
                    <TreeRow title="Pricing" level={2}></TreeRow>
                    <TreeRow title="Contact Us" level={2}></TreeRow>
                </TreeRow>
            </Tree>
        )
    }
}

export default ContentTree;