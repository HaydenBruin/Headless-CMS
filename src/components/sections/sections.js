import React, { Component } from 'react'

import ContentTree from './content-tree';

class SectionTree extends Component {
    render() {
        if(this.props.match.params.section === "content")
        {
            return (
                <ContentTree />
            )
        }
        else if(this.props.match.params.section === "media")
        {
            return (
                <h1>Section {this.props.match.params.section}</h1>
            )
        }
        else if(this.props.match.params.section === "structure")
        {
            return (
                <h1>Section {this.props.match.params.section}</h1>
            )
        }
        else if(this.props.match.params.section === "settings")
        {
            return (
                <h1>Section {this.props.match.params.section}</h1>
            )
        }
        else
        {
            return (
                <h1>nothing</h1>
            )
        }   
    }
}

export default SectionTree;
