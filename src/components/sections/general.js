import React, { Component } from 'react'

export default class General extends Component {
    render() {
        return (
            <div>
                <h1>General: {this.props.match.params.section}</h1>
            </div>
        )
    }
}
